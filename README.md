# Citi Bike Data Analysis

This project analyzes Citi Bike data to gain insights into bike usage patterns in New York City.

## Dataset

The dataset used in this project is provided by Citi Bike, a bike-share program in New York City. The dataset includes information about individual rides such as start and end time, start and end station, trip duration, user type, and gender.

The project uses two datasets:

- `trips.csv`: This dataset contains information about individual trips made on Citi Bikes, including the start and end time and station, as well as the user type and gender.

- `stations.csv`: This dataset contains information about Citi Bike stations, including the latitude and longitude coordinates.

## Analysis

The project analyzes the Citi Bike data to gain insights into bike usage patterns in New York City. It includes visualizations of the most popular bike stations and the busiest times of day, as well as statistical analyses of trip durations. It also includes predictions utilizing the SARIMAX library on future demands.

The code for the analysis is written in Python and uses popular data science libraries such as Pandas, NumPy, and Matplotlib. The visualizations are created using the Folium library, which allows for interactive maps to be generated in Python.

## Conclusion

The analysis provides insights into the usage patterns of Citi Bikes in New York City, which can be used to inform decisions related to bike infrastructure and planning.

